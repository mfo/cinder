#!/bin/sh

set -e

. /usr/share/debconf/confmodule
CINDER_CONF=/etc/cinder/cinder.conf

#PKGOS-INCLUDE#

manage_cinder_my_ip () {
	pkgos_inifile get ${CINDER_CONF} DEFAULT my_ip
	if [ -n "${RET}" ] && [ ! "${RET}" = "NOT_FOUND" ] ; then
		db_set cinder/my-ip "${RET}"
	else
		DEFROUTE_IF=`awk '{ if ( $2 == "00000000" ) print $1 }' /proc/net/route | head -n 1`
		if [ -n "${DEFROUTE_IF}" ] && [ -x /bin/ip ] ; then
			DEFROUTE_IP=$(LC_ALL=C ip addr show "${DEFROUTE_IF}" | grep inet | head -n 1 | awk '{print $2}' | cut -d/ -f1 | grep -E '^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}$' || true)
		fi
		if [ -z "${DEFROUTE_IP}" ] ; then
			HOSTNAME_IP=$(hostname -i)
			if [ -n "${HOSTNAME_IP}" ] ; then
				DEFROUTE_IP=${HOSTNAME_IP}
			fi
		fi
		if [ -n "${DEFROUTE_IP}" ] ; then
			db_set cinder/my-ip ${DEFROUTE_IP}
		fi
	fi
	db_input high cinder/my-ip || true
	db_go
}

manage_glance_api_servers () {
	pkgos_inifile get ${CINDER_CONF} DEFAULT glance_api_servers
	if [ -n "${RET}" ] && [ ! "${RET}" = "NOT_FOUND" ] ; then
		db_set cinder/glance-api-servers "${RET}"
	fi
	db_input high cinder/glance-api-servers || true
	db_go
}

pkgos_var_user_group cinder
pkgos_dbc_read_conf -pkg cinder-common ${CINDER_CONF} database connection cinder $@
pkgos_rabbit_read_conf ${CINDER_CONF} oslo_messaging_rabbit cinder
pkgos_read_admin_creds ${CINDER_CONF} keystone_authtoken cinder

pkgos_inifile get ${CINDER_CONF} lvm volume_group
if [ -n "${RET}" ] && [ ! "${RET}" = "NOT_FOUND" ] ; then
	db_set cinder/volume_group "${RET}"
else
	db_get cinder/volume_group
	if [ -z "${RET}" ] && [ -x /sbin/vgdisplay ] ; then
		# Since we have no prior value, try to guess it from vgdisplay
		VGDISP=`vgdisplay -c | head -n 1`
		if [ -n "${VGDISP}" ] ; then
			VGNAME=`echo ${VGDISP} | cut -d: -f1`
			db_set cinder/volume_group ${VGNAME}
		fi
	fi
fi
db_input high cinder/volume_group || true
db_go
manage_cinder_my_ip

exit 0
